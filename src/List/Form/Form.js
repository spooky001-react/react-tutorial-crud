import React, { Component } from 'react';


class Form extends Component {

 render() {

  

    return (
      <div className="container"> 
      
      <table className="table" ><tbody>
        <tr>
          <td>Lastname:</td><td>  <input name="lastname" onChange={this.props.changeLastname}/></td>
        </tr>
        <tr>
          <td>Email:</td><td>  <input name="email" onChange={this.props.changeEmail}/></td>
        </tr>
        <tr>
          <td>Phone:</td><td>  <input name="phone" onChange={this.props.changePhone}/></td>
        </tr>
        <tr>
          <td colSpan="2"><button className="btn btn-primary btn-large" onClick={this.props.add}>Add</button></td>
        </tr>
      </tbody></table>
      
     </div>

    );
  }


}
export default Form;